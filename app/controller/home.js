'use strict';

const Controller = require('egg').Controller;

class ubanModuleController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, i am a module';
  }
}

module.exports = ubanModuleController;
